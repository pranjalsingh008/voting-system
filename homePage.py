import subprocess as sub
from tkinter import *
from admin import adminLogin
from voter import voterLogin


def Home(home, frame1, frame2):

    for frame in home.winfo_children():
        for widget in frame.winfo_children():
            widget.destroy()

    Button(frame2, text="Home", command = lambda: Home(home, frame1, frame2)).grid(row=0,column=0)
    Label(frame2, text="                                                                          ").grid(row = 0,column = 1)
    Label(frame2, text="                                                                          ").grid(row = 0,column = 2)
    Label(frame2, text="                                                                          ").grid(row = 1,column = 1)
    frame2.pack(side=TOP)

    home.title("Home")

    Label(frame1, text="Home", font=('Times New Roman', 25, 'bold')).grid(row = 0, column = 1, columnspan=2)
    Label(frame1, text="").grid(row = 1,column = 0)

    admin = Button(frame1, text="Admin Login", width=15, command = lambda: adminLogin(home, frame1))
    voter = Button(frame1, text="Voter Login", width=15, command = lambda: voterLogin(home, frame1))
    newTab = Button(frame1, text="New Window", width=15, command = lambda: sub.call('gnome-terminal --command="python3 homePage.py"', shell=True))

    Label(frame1, text="").grid(row = 2,column = 0)
    Label(frame1, text="").grid(row = 4,column = 0)
    Label(frame1, text="").grid(row = 6,column = 0)
    admin.grid(row = 3, column = 1, columnspan = 2)
    voter.grid(row = 5, column = 1, columnspan = 2)
    newTab.grid(row = 7, column = 1, columnspan = 2)

    frame1.pack()
    home.mainloop()


def newHome():
    home = Tk()
    home.geometry('500x500')
    frame1 = Frame(home)
    frame2 = Frame(home)
    Home(home, frame1, frame2)


if __name__ == "__main__":
    newHome()
