import subprocess as sub
import tkinter as tk
import registerVoter as regV
import admFunc as adFunc
from tkinter import *
from registerVoter import *
from admFunc import *


def AdminHome(root,frame1,frame3):
    root.title("Admin")
    for widget in frame1.winfo_children():
        widget.destroy()

    Button(frame3, text="Admin", command = lambda: AdminHome(root, frame1, frame3)).grid(row = 1, column = 0)
    frame3.pack(side=TOP)

    Label(frame1, text="Admin", font=('Helvetica', 25, 'bold')).grid(row = 0, column = 1,columnspan = 2)
    Label(frame1, text="").grid(row = 1,column = 0)

    runServer = Button(frame1, text="Run Server", width=15, command = lambda: sub.call('gnome-terminal --command="python3 Server.py"', shell=True))

    registerVoter = Button(frame1, text="Register Voter", width=15, command = lambda: regV.Register(root, frame1))

    showVotes = Button(frame1, text="Show Votes", width=15, command = lambda: adFunc.showVotes(root, frame1))

    reset = Button(frame1, text="Reset All", width=15, command = lambda: adFunc.resetAll(root, frame1))

    Label(frame1, text="").grid(row = 2,column = 0)
    Label(frame1, text="").grid(row = 4,column = 0)
    Label(frame1, text="").grid(row = 6,column = 0)
    Label(frame1, text="").grid(row = 8,column = 0)
    runServer.grid(row = 3, column = 1, columnspan = 2)
    registerVoter.grid(row = 5, column = 1, columnspan = 2)
    showVotes.grid(row = 7, column = 1, columnspan = 2)
    reset.grid(row = 9, column = 1, columnspan = 2)


    frame1.pack()
    root.mainloop()


def admin_cred(root,frame1,admin_ID,password):

    if(admin_ID=="admin" and password=="admin"):
        frame3 = root.winfo_children()[1]
        AdminHome(root, frame1, frame3)
    else:
        msg = Message(frame1, text="Incorrect ID or Password", width=500)
        msg.grid(row = 6, column = 0, columnspan = 5)


def adminLogin(root,frame1):

    root.title("Admin Login")

    for widget in frame1.winfo_children():
        widget.destroy()

    Label(frame1, text="Admin Login", font=('Helvetica', 18, 'bold')).grid(row = 0, column = 2, columnspan=1)
    Label(frame1, text="").grid(row = 1,column = 2)
    Label(frame1, text="Admin ID:      ", anchor="e").grid(row = 2,column = 0)
    Label(frame1, text="Password:       ", anchor="e").grid(row = 3,column = 0)

    admin_ID = tk.StringVar()
    password = tk.StringVar()

    e1 = Entry(frame1, textvariable = admin_ID)
    e1.grid(row = 2,column = 2)
    e2 = Entry(frame1, textvariable = password, show = '*')
    e2.grid(row = 3,column = 2)

    login = Button(frame1, text="Login", width=10, command = lambda: admin_cred(root, frame1, admin_ID.get(), password.get()))
    Label(frame1, text="").grid(row = 4,column = 0)
    login.grid(row = 5, column = 3, columnspan = 2)

    frame1.pack()
    root.mainloop()
